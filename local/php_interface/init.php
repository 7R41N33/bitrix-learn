<?php

use Bitrix\Main\Loader;

require "constants.php";

Loader::includeModule("iblock");
Loader::includeModule("vacancies");

//\Bitrix\Vacancies\Agent\Vacancy::setNonActive();

//$vacancies = \Bitrix\Vacancies\Entity\Vacancy::getList();

//CModule::AddAutoloadClasses(
//    "",
//    [
//        "Local\\Agent\\Vacancy" => "/local/php_interface/include/agent/vacancy.php",
//    ]
//);

AddEventHandler('main', 'OnEndBufferContent', 'controller404', 1001);

function controller404(&$content) {
    if (defined('ERROR_404') && ERROR_404 == 'Y') {
        $content = "404";
        return;
    }
}
