<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$APPLICATION->IncludeComponent(
    'vacancy:vacancies.detail',
    '',
    [
        'ID' => $arResult["VARIABLES"]["ELEMENT_ID"],
        'SEF_FOLDER' => $arParams['SEF_FOLDER'],
    ],
    $component
);
?>