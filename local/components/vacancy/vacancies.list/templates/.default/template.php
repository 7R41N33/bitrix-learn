<?php

/**
 * @var array $arResult
 */

?>

<? if (empty($arResult["VACANCIES"])) { ?>
    Список вакансий пуст
    <? return; ?>
<? } ?>

<h1>Список вакансий</h1>

<form action="" method="post">
    <input name="ACTIVE_FROM_BEGIN" value="">
    <input name="ACTIVE_FROM_END" value="">

    <input type="submit" value="Применить">
</form>

<? foreach ($arResult["VACANCIES"] as $vacancy) { ?>
    <p>
        <a href="<?= $vacancy["DETAIL_PAGE_URL"]; ?>"><?= $vacancy["NAME"]; ?></a>
        <span><?= $vacancy["PROPERTY_EMPLOYER_NAME"]; ?></span>
    </p>
<? } ?>
