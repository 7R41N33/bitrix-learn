<?php

class VacanciesListComponent extends CBitrixComponent
{
    protected $filter;

    public function executeComponent()
    {
        $this->prepareFilter();
        $this->prepareVacancies();

        $this->IncludeComponentTemplate();
    }

    protected function prepareFilter()
    {
        $this->filter = [
            "IBLOCK_ID" => VACANCY_IBLOCK_ID,
        ];

        if (isset($_POST["ACTIVE_FROM"])) {
            $this->filter[">DATE_ACTIVE_FROM"] = $_POST["ACTIVE_FROM_BEGIN"];
            $this->filter["<DATE_ACTIVE_FROM"] = $_POST["ACTIVE_FROM_END"];
        }
    }

    protected function prepareVacancies()
    {
        $iblockResult = CIBlockElement::GetList(
            [],
            $this->filter,
            false,
            false,
            [
                "ID",
                "NAME",
                "DETAIL_PAGE_URL",
                "PROPERTY_EMPLOYER.NAME",
            ]
        );

        while ($vacancy = $iblockResult->GetNext()) {
            $this->arResult["VACANCIES"][] = $vacancy;
        }
    }
}
