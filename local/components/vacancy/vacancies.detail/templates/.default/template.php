<?php

/**
 * @var array $arResult
 */

?>

<h1><?= $arResult["VACANCY"]["NAME"]; ?></h1>

<? if (!empty($arResult["VACANCY"]["DETAIL_PICTURE"])) { ?>
<p>
    <img src="<?= $arResult["VACANCY"]["DETAIL_PICTURE"]["SRC"]; ?>">
</p>
<? } ?>

<p>
    <?= $arResult["VACANCY"]["EMPLOYER"]["NAME"]; ?>
</p>

<p>
    <?= $arResult["VACANCY"]["DESCRIPTION"]; ?>
</p>
