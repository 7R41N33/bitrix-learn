<?php

class VacanciesDetailComponent extends CBitrixComponent
{
    /**
     * @var int
     */
    protected $vacancyId;

    public function onPrepareComponentParams($arParams)
    {
        if (isset($arParams["ID"])) {
            $this->vacancyId = (int) $arParams["ID"];
        }
    }

    public function executeComponent()
    {
        if (!$this->vacancyId) {
            LocalRedirect("/vacancy-standard/");
        }

        $this->prepareVacancy();

        $this->IncludeComponentTemplate();
    }

    public function prepareVacancy()
    {
        $vacancy = CIBlockElement::GetList(
            [],
            [
                "ID"        => $this->vacancyId,
                "IBLOCK_ID" => VACANCY_IBLOCK_ID,
            ],
            false,
            false,
            [
                "ID",
                "NAME",
                "DESCRIPTION",
                "DETAIL_PICTURE",
                "PROPERTY_EMPLOYER.NAME",
            ]
        )->GetNext();

        if (!$vacancy) {
            LocalRedirect("/404.php");
        }

        $result = [
            "NAME" => $vacancy["NAME"],
            "DESCRIPTION" => $vacancy["DESCRIPTION"],
            "EMPLOYER" => [
                "NAME" => $vacancy["PROPERTY_EMPLOYER_NAME"],
            ],
        ];

        if ($vacancy["DETAIL_PICTURE"] > 0) {
            $result["DETAIL_PICTURE"] = \CFile::GetFileArray($vacancy["DETAIL_PICTURE"]);
        } else {
            $result["DETAIL_PICTURE"] = [];
        }

        $this->arResult["VACANCY"] = $result;
    }
}
