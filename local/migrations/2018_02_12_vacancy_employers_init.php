<?php

use Bitrix\Main\Loader;

require __DIR__ . '/../modules/vacancies/include/migration.php';

Loader::includeModule('iblock');

$vacancyIblockTypeCode = 'vacancy';
$vacancyIblockType = CIBlockType::GetByID($vacancyIblockTypeCode)->Fetch();

if (!$vacancyIblockType) {
    $type = new CIBlockType();
    $result = $type->Add([
        'ID' => $vacancyIblockTypeCode,
        'SECTIONS' => 'Y',
        'IN_RSS' => 'N',
        'SORT' => 100,
        'LANG' => [
            'ru' => [
                'NAME' => 'Вакансии',
                'SECTION_NAME' => 'Вакансии',
                'ELEMENT_NAME' => 'Вакансия',
            ],
            'en' => [
                'NAME' => 'Vacancies',
                'SECTION_NAME' => 'Vacancies',
                'ELEMENT_NAME' => 'Vacancy',
            ],
        ],
    ]);

    if (!$result) {
        echo $type->LAST_ERROR . PHP_EOL;
        die();
    } else {
        echo 'Vacancy iblock type was created' . PHP_EOL;
    }
} else {
    echo 'Vacancy iblock type is exist' . PHP_EOL;
}

$iblockVacancyCode = 'vacancy';
$vacancyIblock = \CIBlock::GetList(
    ['id' => 'asc'],
    [
        'CODE' => $iblockVacancyCode,
        'IBLOCK_TYPE_ID' => $vacancyIblockTypeCode,
        'CHECK_PERMISSIONS' => 'N',
    ])
    ->Fetch();

if (!$vacancyIblock) {
    $newVacancyIblock = new CIBlock();
    $result = $newVacancyIblock->Add([
        'ACTIVE' => 'Y',
        'NAME' => 'Вакансии',
        'CODE' => $iblockVacancyCode,
        'IBLOCK_TYPE_ID' => $vacancyIblockTypeCode,
        'SITE_ID' => ['s1'],
        'SORT' => 100,
        'CHECK_PERMISSIONS' => 'N',
    ]);

    if (!$result) {
        echo $newVacancyIblock->LAST_ERROR . PHP_EOL;
        die();
    } else {
        $vacancyIblock['ID'] = $result['ID'];

        echo 'Vacancy iblock was created' . PHP_EOL;
    }
} else {
    echo 'Vacancy iblock is exist: id = ' . $vacancyIblock['ID'] . PHP_EOL;
}


$employerIblockTypeCode = 'employer';
$employerIblockType = CIBlockType::GetByID($employerIblockTypeCode)->Fetch();

if (!$employerIblockType) {
    $type = new CIBlockType();
    $result = $type->Add([
        'ID' => $employerIblockTypeCode,
        'SECTIONS' => 'Y',
        'IN_RSS' => 'N',
        'SORT' => 100,
        'LANG' => [
            'ru' => [
                'NAME' => 'Работодатели',
                'SECTION_NAME' => 'Работодатели',
                'ELEMENT_NAME' => 'Работодатель',
            ],
            'en' => [
                'NAME' => 'Employers',
                'SECTION_NAME' => 'Employers',
                'ELEMENT_NAME' => 'Employer',
            ],
        ],
    ]);

    if (!$result) {
        echo $type->LAST_ERROR . PHP_EOL;
        die();
    } else {
        echo 'Employer iblock type was created' . PHP_EOL;
    }
} else {
    echo 'Employer iblock type is exist' . PHP_EOL;
}

$iblockEmployerCode = 'employer';
$employerIblock = \CIBlock::GetList(
    ['id' => 'asc'],
    [
        'CODE' => $iblockEmployerCode,
        'IBLOCK_TYPE_ID' => $employerIblockTypeCode,
        'CHECK_PERMISSIONS' => 'N',
    ])
    ->Fetch();

if (!$employerIblock) {
    $newEmployerIblock = new CIBlock();
    $result = $newEmployerIblock->Add([
        'ACTIVE' => 'Y',
        'NAME' => 'Работодатели',
        'CODE' => $iblockEmployerCode,
        'IBLOCK_TYPE_ID' => $employerIblockTypeCode,
        'SITE_ID' => ['s1'],
        'SORT' => 100,
        'CHECK_PERMISSIONS' => 'N',
    ]);

    if (!$result) {
        echo $newEmployerIblock->LAST_ERROR . PHP_EOL;
        die();
    } else {
        $employerIblock['ID'] = $result['ID'];

        echo 'Employer iblock was created' . PHP_EOL;
    }
} else {
    echo 'Employer iblock is exist: id = ' . $employerIblock['ID'] . PHP_EOL;
}

$newProperties = [
    [
        'NAME' => 'Работодатель',
        'ACTIVE' => 'Y',
        'SORT' => 100,
        'CODE' => 'EMPLOYER',
        'IS_REQUIRED' => 'Y',
        'PROPERTY_TYPE'  => 'E',
        'LINK_IBLOCK_ID' => $employerIblock['ID'],
        'IBLOCK_ID' => $vacancyIblock['ID'],
    ],
    [
        'NAME'          => 'Тип работы',
        'ACTIVE'        => 'Y',
        'SORT'          => 100,
        'CODE'          => 'TYPE',
        'PROPERTY_TYPE' => 'L',
        'LIST_TYPE'     => 'C',
        'VALUES'        => [
            [
                'VALUE'  => 'Полный рабочий день',
                'DEF'    => 'Y',
                'SORT'   => '200',
                'XML_ID' => 'FULL_TIME',
            ],
            [
                'VALUE'  => 'Удаленная',
                'DEF'    => 'N',
                'SORT'   => '100',
                'XML_ID' => 'REMOTE',
            ],
        ],
        'IS_REQUIRED'   => 'Y',
        'IBLOCK_ID'     => $vacancyIblock['ID'],
    ],
    [
        'NAME'          => 'Зарплата от',
        'ACTIVE'        => 'Y',
        'SORT'          => 100,
        'CODE'          => 'SALARY_FROM',
        'PROPERTY_TYPE' => 'N',
        'IBLOCK_ID'     => $vacancyIblock['ID'],
    ],
    [
        'NAME'          => 'Зарплата до',
        'ACTIVE'        => 'Y',
        'SORT'          => 100,
        'CODE'          => 'SALARY_TO',
        'PROPERTY_TYPE' => 'N',
        'IBLOCK_ID'     => $vacancyIblock['ID'],
    ],
    [
        'NAME'          => 'График работы',
        'ACTIVE'        => 'Y',
        'SORT'          => 100,
        'CODE'          => 'SCHEDULE',
        'PROPERTY_TYPE' => 'S',
        'IBLOCK_ID'     => $vacancyIblock['ID'],
    ],
    [
        'NAME'          => 'Тестовое задание',
        'ACTIVE'        => 'Y',
        'SORT'          => 100,
        'CODE'          => 'TEST',
        'PROPERTY_TYPE' => 'F',
        'IBLOCK_ID'     => $vacancyIblock['ID'],
    ]
];

foreach ($newProperties as $newProperty) {
    $existingProperty = CIBlockProperty::GetList(
        [],
        [
            'IBLOCK_ID' => $vacancyIblock['ID'],
            'CODE' => $newProperty['CODE'],
        ]
    )->Fetch();

    if (!$existingProperty) {
        $property = new CIBlockProperty();
        $result = $property->Add($newProperty);

        if (!$result) {
            echo $property->LAST_ERROR . PHP_EOL;
        } else {
            echo 'Property was created: code = ' . $newProperty['CODE'] . PHP_EOL;
        }
    } else {
        echo 'Vacancy property is exist: code = ' . $existingProperty['CODE'] . PHP_EOL;
    }
}
