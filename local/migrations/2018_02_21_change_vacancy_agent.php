<?php

require __DIR__ . '/../modules/vacancies/include/migration.php';

CAgent::RemoveAgent("Local\Agent\Vacancy::setNonActive();", "main");

CAgent::AddAgent(
    "\Bitrix\Vacancies\Agent\Vacancy::setNonActive();",
    "main",
    "N",
    60,
    "",
    "Y",
    "",
    30
);