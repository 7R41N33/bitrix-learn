<?

/**
 * @var array $arResult
 */

?>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Вакансии</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <? foreach ($arResult as $menuItem) { ?>
                    <?
                        if ($menuItem["DEPTH_LEVEL"] !== 1) {
                            continue;
                        }
                    ?>
                    <li class="<?= ($menuItem["SELECTED"]) ? "active" : ""; ?>"><a href="<?= $menuItem["LINK"]?>"><?= $menuItem["TEXT"]?></a></li>
                <? } ?>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
