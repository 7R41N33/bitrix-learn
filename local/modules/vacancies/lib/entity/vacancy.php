<?php

namespace Bitrix\Vacancies\Entity;

class Vacancy extends BaseModel
{
    const CACHE_PATH = 'vacancies';

    public static function getList($sort = [], $filter = [])
    {
        $result = [];

        $cache = new \CPHPCache();
        $iblock_id = static::getIblockId();
        $cache_id = 'elements_' . $iblock_id . md5(serialize($sort)) . md5(serialize($filter));
        $cache_dir = static::CACHE_PATH . '/elements';

        if ($cache->InitCache(static::CATCH_TIME_ELEMENT_DEFAULT, $cache_id, $cache_dir)) {
            $result = $cache->GetVars();
        } elseif ($cache->StartDataCache()) {
            $filter['IBLOCK_ID'] = $iblock_id;
            $filter['ACTIVE'] = 'Y';

            $vacancies = \CIBlockElement::GetList($sort, $filter);

            while ($vacancy = $vacancies->GetNextElement()) {
                $data = $vacancy->GetFields();
                $data['PROPERTIES'] = $vacancy->GetProperties();
                $result[$data['ID']] = static::getInstance($data['ID'], $data);
            }

            if (!empty($result)) {
                global $CACHE_MANAGER;
                $CACHE_MANAGER->StartTagCache($cache_dir);
                $CACHE_MANAGER->RegisterTag('iblock_id_'. $iblock_id);
                $CACHE_MANAGER->EndTagCache();
                $cache->EndDataCache($result);
            } else {
                $cache->AbortDataCache();
            }
        }

        return $result;
    }

    public static function getIblockId()
    {
        return VACANCY_IBLOCK_ID;
    }
}
