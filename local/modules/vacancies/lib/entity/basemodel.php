<?php

namespace Bitrix\Vacancies\Entity;

use Bitrix\Vacancies\Interfaces\Entity;

abstract class BaseModel implements Entity
{
    const CATCH_TIME_ELEMENT_DEFAULT = 864000;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var array
     */
    protected $data;

    public function __construct($id = 0, $data = [])
    {
        $this->id = $id;
        $this->data = $data;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    public function load()
    {
        if (empty($this->data)) {
            $element = \CIBlockElement::GetByID($this->getId())->GetNextElement();

            if ($element) {
                $this->data = $element->GetFields();
                $this->data["PROPERTIES"] = $element->GetProperties();
            }
        }
    }

    /**
     * @param  string $fieldName
     * @return mixed
     */
    protected function getField($fieldName)
    {
        if (empty($this->data[$fieldName])) {
            $this->load();
        }

        return $this->data[$fieldName];
    }

    /**
     * @param  string $propertyCode
     * @return mixed
     */
    protected function getPropertyValue($propertyCode)
    {
        if (!isset($this->data['PROPERTIES'][$propertyCode]['VALUE'])) {
            $this->load();
        }

        return $this->data['PROPERTIES'][$propertyCode]['VALUE'];
    }

    /**
     * @return bool
     */
    public function exists()
    {
        $this->load();

        return (!empty($this->data));
    }

    /**
     * @param  int   $id
     * @param  array $data
     * @return static
     */
    public static function getInstance($id, $data)
    {
        return new static($id, $data);
    }
}
