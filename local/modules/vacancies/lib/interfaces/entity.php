<?php

namespace Bitrix\Vacancies\Interfaces;

interface Entity
{
    /**
     * @param  array $sort
     * @param  array $filter
     * @return array
     */
    public static function getList($sort = [], $filter = []);

    /**
     * @return int
     */
    public static function getIblockId();
}
