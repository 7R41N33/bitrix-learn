<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Application;

Loc::loadMessages(__FILE__);

class vacancies extends CModule
{
    public $MODULE_ID;
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $MODULE_GROUP_RIGHTS;

    public function __construct()
    {
        $this->MODULE_ID = "vacancies";
        $this->MODULE_VERSION = "1.0";
        $this->MODULE_VERSION_DATE = date("d.m.Y H:i:s");
        $this->MODULE_NAME = Loc::getMessage('MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('MODULE_DESCRIPTION');
        $this->MODULE_GROUP_RIGHTS = 'N';
    }

    public function doInstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION;

        ModuleManager::registerModule($this->MODULE_ID);
        $this->installTables();
        $this->installFiles();

        $APPLICATION->IncludeAdminFile(
            "Установка модуля vacancies",
            $DOCUMENT_ROOT."/local/modules/vacancies/install/step.php"
        );
    }

    public function doUninstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION;

        $this->uninstallFiles();
        $this->uninstallTables();
        ModuleManager::unregisterModule($this->MODULE_ID);

        $APPLICATION->IncludeAdminFile(
            "Деинсталляция модуля vacancies",
            $DOCUMENT_ROOT."/local/modules/vacancies/install/unstep.php"
        );
    }

    public function installTables()
    {
    }

    public function installFiles()
    {
    }

    public function uninstallTables()
    {
    }

    public function uninstallFiles()
    {
    }
}
